﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class BusinessLocation
    {
        public string businessName { get; set; }
        public string branchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int visitorsNow { get; set; }
        public BusinessLocation() { }
        public BusinessLocation(string bn, string bc, int mc, int vn)
        {
            businessName = bn;
            branchCode = bc;
            MaximumCapacity = mc;
            visitorsNow = vn;
        }
        public override string ToString()
        {
            return "Business Name:" + businessName +
                "\t Branch Code" + branchCode +
                "\t Maximum Capacity" + MaximumCapacity +
                "\t Visitors Now" + visitorsNow;
        }
        public bool IsFull()
        {

            if (visitorsNow == MaximumCapacity)
            {
                Console.WriteLine("This business location is full,please try another location");
                return true;
            }
            else
                return false;



        }
    }
}
