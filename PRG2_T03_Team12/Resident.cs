﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class Resident : Person //attributes
    {
        public string address { get; set; }
        public DateTime lastLeftCountry { get; set; }
        public TraceTogetherToken token { get; set; }
        //end of attributes 
        public Resident(string n, string addr, DateTime llC) : base(n) //Constructors
        {
            address = addr;
            lastLeftCountry = llC;

        }
        public override string ToString() //Tostring() Method
        {
            return "Name: " + Name +
                "\t Last left country: " + lastLeftCountry +
                "\t Trace Together Token: " + token;

        }
        public override double CalculateSHNCharges() //Calculate SHNCharges Method for resident
        {
            double total = 0;
            foreach (TravelEntry e in travelEntryList)
            {

                int days = e.CalculateSHNDuration();
                if (days == 7)
                {

                    total += 220;
                }
                else if (days == 14)
                {

                    total += 1220;
                }
            }
            return total;
        }
    }
}
