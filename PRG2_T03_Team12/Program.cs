﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> personList = new List<Person>(); //Creates new list to store person
            List<Resident> residentlist = new List<Resident>(); //Creates new list to store residents
            List<Visitor> visitorlist = new List<Visitor>(); //Creates new list to store visitors
            List<BusinessLocation> BusinessLocationlist = new List<BusinessLocation>(); //Creates new list to store Business Location
            List<SHNFacility> SHNFacilityList = new List<SHNFacility>(); //Creates new list to SHNFacilities
            InitSHNFacilityList(ref SHNFacilityList); //Initialize SHN Facilities list
            InitPersonList(personList, visitorlist, residentlist); //Initialize Person list
            InitBusinessLocation(BusinessLocationlist); //Initialize Businesslocation list
            Menu(personList, BusinessLocationlist, SHNFacilityList, residentlist, visitorlist); //Call menu method at the start of the program
            static void InitSHNFacilityList(ref List<SHNFacility> SHNFacilityList) //Method to call api and store in SHN Facilities list
            {
                using (HttpClient client = new HttpClient())
                {

                    client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net"); //Line to Read Api

                    Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");

                    responseTask.Wait();

                    HttpResponseMessage result = responseTask.Result;

                    if (result.IsSuccessStatusCode)

                    {
                        Task<string> readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();

                        string data = readTask.Result;

                        SHNFacilityList = JsonConvert.DeserializeObject<List<SHNFacility>>(data);  //Loads Api data into SHNFacility list
                    }
                }
            }
            static void DisplaySHNFacilityList(List<SHNFacility> SHNFacilityList) //Method to display SHN Facilities from SHNFacility list
            {
                Console.WriteLine("{0,-20} {1,-20} {2,-20} {3,-25}", "Facility Name", "Facility Capacity", "Dist from air checkpoint", "Dist from sea checkpoint", "Distance from land checkpoint");
                foreach (SHNFacility s in SHNFacilityList)
                {
                    Console.WriteLine("{0,-20} {1,-20} {2,-24} {3,-20}", s.FacilityName, s.FacilityCapacity, s.DistFromAirCheckPoint, s.DistFromSeaCheckPoint, s.DistFromLandCheckPoint);
                }
            }
            static void Menu(List<Person> personList, List<BusinessLocation> BusinessLocationlist, List<SHNFacility> SHNFacilityList, List<Resident> residentlist, List<Visitor> visitorlist) //Method to display main menu
            {
                while (true) //While loop to loop through menu after each user input
                {
                    try //Try/catch to find out if user input is correct
                    {
                        Console.WriteLine("Welcome to the Covid-19 Monitoring system");
                        Console.WriteLine("=================Menu====================");
                        Console.WriteLine("[1] Load Person data");
                        Console.WriteLine("[2] Load Business Location Data");
                        Console.WriteLine("[3] Load SHNFacility data");
                        Console.WriteLine("[4] List Person details");
                        Console.WriteLine("[5] List all Visitors");
                        Console.WriteLine("[6] Load Contact tracing report");
                        Console.WriteLine("[7] Load Status report");
                        Console.WriteLine("[8] SafeEntry Menu");
                        Console.WriteLine("[9] TravelEntry Menu");
                        Console.WriteLine("Please choose an option:");
                        int Option = Convert.ToInt32(Console.ReadLine());
                        if (Option == 1) //Call method to load person csv data into personList
                        {
                            InitPersonList(personList, visitorlist, residentlist); //Initialize Person list
                            Console.WriteLine("Person data loaded into personList"); 
                        }
                        else if (Option == 2) //Call method to load Business Location data into BusinessLocationlist
                        {
                            InitBusinessLocation(BusinessLocationlist);
                            Console.WriteLine("Business Location data loaded into BusinessLocationlist");
                        }
                        else if (Option == 3) //Call method to load SHNFacility data from api into SHNFacility list
                        {
                            InitSHNFacilityList(ref SHNFacilityList);
                            Console.WriteLine("SHN Facility data loaded into SHNFacilitylist");
                        }
                        else if (Option == 4) //Call method to load person details (visitor or resident)
                        {
                            loadpersondetails(personList, visitorlist, residentlist);
                        }
                        else if (Option == 5) //Call method to list all visitors in visitorlist
                        {
                            Listallvisitors(visitorlist,personList);
                        }
                        else if (Option == 6) //Call method to create a new CSV file to store Contact Tracing Reporting (Advanced Feature)
                        {
                            CreateContactTracingReportingCSV(visitorlist, BusinessLocationlist, personList);
                        }
                        else if (Option == 7) //Call method to create a new CSV file to store status reports (Advanced Feature)
                        {
                            CreateStatusReport(SHNFacilityList, personList);
                        }
                        else if (Option == 8) //Call Method to display SafeEntryMenu
                        {
                            SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                        }
                        else if (Option == 9) //Call Method to display TravelEntryMenu
                        {
                            TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                        }
                        else //Simple validation
                        {
                            Console.WriteLine("Error: That is not an option, please try again");
                        }

                    }
                    catch (FormatException ex)
                    {
                        Console.WriteLine("Error: You have entered the wrong format, please try again");
                    }
                }

            }
            static void SafeEntryMenu(List<SHNFacility> SHNFacilityList, List<Person> personList, List<Visitor> visitorlist, List<BusinessLocation> BusinessLocationlist, List<Resident> residentlist) //Method to display SafeEntry Menu
            {
                try //Try and catch to check if user input is correct
                {
                    Console.WriteLine("==========SafeEntry Menu==========");
                    Console.WriteLine("[1] Assign/replace TraceTogether Tokens: ");
                    Console.WriteLine("[2] List all Business Locations:  ");
                    Console.WriteLine("[3] Edit Business Location Capacity:");
                    Console.WriteLine("[4] SafeEntry Check In: ");
                    Console.WriteLine("[5] SafeEntry Check Out: ");
                    Console.WriteLine("[6] TravelEntry Menu");
                    Console.WriteLine("[7] Main menu");

                    Console.WriteLine("Enter your option: ");
                    int Option2 = Convert.ToInt32(Console.ReadLine());
                    if (Option2 == 1)  //Call AssignReplaceToken method to allow user to assign a tracetogehter token to a resident or replace a token
                    {
                        AssignReplaceToken(residentlist);
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (Option2 == 2)  // Call DisplayBusinessLocation method to display list of Business Locations
                    {
                        DisplayBusinessLocationlist(BusinessLocationlist); CreateStatusReport(SHNFacilityList, personList);
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    } 
                    else if (Option2 == 3) //Call EditBusinessCapacity to allow users to edit user to edit a specific business location's maximum capacity
                    {
                        EditBusinessCapacity(BusinessLocationlist);
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (Option2 == 4)  //Call safeEntry check in method to allow user to check in
                    {
                        SafeEntryCheckIn(personList, BusinessLocationlist);
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (Option2 == 5) //Call SafeEntry CheckOut method to allow users to perform checkout
                    {
                        SafeEntryCheckOut(personList, BusinessLocationlist);
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (Option2 == 6) //Call Method to display TravelEntryMenu
                    {
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (Option2 == 7)// Allow users to access main menu
                    {
                        Menu(personList, BusinessLocationlist, SHNFacilityList, residentlist, visitorlist);
                    }
                    else //Simple validation
                    {
                        Console.WriteLine("That is an invalid Option, please try again");
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }

                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                    SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                }
            }
            static void TravelEntryMenu(List<SHNFacility> SHNFacilityList, List<Person> personList, List<Visitor> visitorlist, List<BusinessLocation> BusinessLocationlist, List<Resident> residentlist) //Method to display TravelEntry Menu
            {
                try //Try and Catch for possible wrong inputs by user
                {
                    Console.WriteLine("==========TravelEntry Menu==========");
                    Console.WriteLine("[1] List all SHN Facilities");
                    Console.WriteLine("[2] Create Visitor");
                    Console.WriteLine("[3] Create TravelEntry Record");
                    Console.WriteLine("[4] Calculate SHN charges");
                    Console.WriteLine("[5] SafeEntry Menu");
                    Console.WriteLine("[6] Main menu");
                    Console.WriteLine("Enter your Option: ");
                    int TEoption = Convert.ToInt32(Console.ReadLine());
                    if (TEoption == 1) //Call method to display SHN Facilities in SHN Facility list
                    {
                        DisplaySHNFacilityList(SHNFacilityList);
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (TEoption == 2) //Call method to allow user to create new visitor
                    {
                        CreateVisitor(personList, visitorlist);
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (TEoption == 3) //Call method to allow user to create a new travel entry record for a resident or visitor
                    {
                        CreateTravelEntryRecord(personList, SHNFacilityList);
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (TEoption == 4) //Call method to allow user to calculate their SHNCharges based on criterion given
                    {
                        CalculateSHNCharges(personList);
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (TEoption == 5) //Call method to display SafeEntry menu
                    {
                        SafeEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                    else if (TEoption == 6) //Allow users to access Main Menu
                    {
                        Menu(personList, BusinessLocationlist, SHNFacilityList, residentlist, visitorlist);
                    }
                    else //Simple Validation
                    {
                        Console.WriteLine("That is an invalid option, please try again");
                        TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                    TravelEntryMenu(SHNFacilityList, personList, visitorlist, BusinessLocationlist, residentlist);
                }
            }
            static void InitPersonList(List<Person> personList, List<Visitor> visitorlist, List<Resident> residentlist) //Method to load data from Person.csv file into the person list
            {
                string[] lineArray = File.ReadAllLines("Person.csv"); //Line to read CSV
                for (int i = 1; i < lineArray.Length; i++)
                {
                    string[] dataArray = lineArray[i].Split(',');
                    string type = dataArray[0];
                    string Name = dataArray[1];
                    if (type == "visitor") //Check if datatype is visitor
                    {
                        string passportNo = dataArray[4]; //Initialize data for visitor, passportNo,Nationality
                        string nationality = dataArray[5];
                        Visitor v = new Visitor(Name, passportNo, nationality); //Create Vistor object to store data
                        if (dataArray[9] != "") //Check if the visitor has data for a travel entry record
                        {
                            string telc = dataArray[9];
                            string tem = dataArray[10];
                            DateTime ted = Convert.ToDateTime(dataArray[11]);
                            string fn = dataArray[14];
                            TravelEntry entry = new TravelEntry(telc, tem, ted); //Create new Travel Entry object
                            entry.ShnEndDate = DateTime.ParseExact(dataArray[12], "dd/MM/yyyy H:mm", null);
                            if (dataArray[13] == "true")
                            {
                                entry.IsPaid = true;
                                
                            }
                            v.travelEntryList.Add(entry);
                        }
                        visitorlist.Add(v); //Add visitor object into visitorlist
                        personList.Add(v); //Add visitor object into personList 
                    }
                    else  //Check if the person is a resident
                    {
                        string addr = dataArray[2]; //Initialize data for resident, lastleftcountry,name,address
                        DateTime lastLeftCountry = Convert.ToDateTime(dataArray[3]);
                        Resident r = new Resident(Name, addr, lastLeftCountry); //Create new resident object
                        if (dataArray[9] != "") //Check if the resident has data for a travel entry record 
                        {
                            string telc = dataArray[9];
                            string tem = dataArray[10];
                            DateTime ted = Convert.ToDateTime(dataArray[11]);
                            DateTime tsed = Convert.ToDateTime(dataArray[12]);
                            bool tip = Convert.ToBoolean(dataArray[13]);
                            string stay = dataArray[14];
                            TravelEntry entry = new TravelEntry(telc, tem, ted); //Create new Travel Entry object
                            r.travelEntryList.Add(entry); //Add new TravelEntry record to residentlist
                            entry.ShnEndDate = (!string.IsNullOrEmpty(dataArray[12])) ? Convert.ToDateTime(dataArray[12]) : DateTime.Now; ;
                            entry.IsPaid = (!string.IsNullOrEmpty(dataArray[13])) ? Convert.ToBoolean(dataArray[13]) : false;
                            if (dataArray[13] == "true")
                            {
                                entry.IsPaid = true;

                            }

                        }
                        string sNo = dataArray[6];
                        string cLocation = dataArray[7];
                        if (dataArray[8] != "") //Check if resident has data for tracetogether token
                        {
                            DateTime exDate = Convert.ToDateTime(dataArray[8]);
                            TraceTogetherToken token = new TraceTogetherToken(sNo, cLocation, exDate); //Create new tracetogehter token object
                            r.token = token; //Initialize tracetogether token object
                        }
                        residentlist.Add(r); //Add resident object to residentlist
                        personList.Add(r); //Add resident object to personList
                    }
                }
            }
            static void InitBusinessLocation(List<BusinessLocation> BusinessLocationlist)  //Method to load data from BusinessLocation.csv file into BusinessLocationlist
            {
                string[] lines = File.ReadAllLines("BusinessLocation.csv"); //Line to read CSV
                for (int i = 1; i < lines.Length; i++)
                {
                    string[] data = lines[i].Split(',');
                    string bn = data[0];     //Initialize data for Business Location
                    string bc = data[1];
                    int mc = Convert.ToInt32(data[2]);
                    int vn = Convert.ToInt32(data[3]);
                    BusinessLocation bl = new BusinessLocation(bn, bc, mc, vn); //Create new Business Location Object
                    BusinessLocationlist.Add(bl); //Add Business Location object into BusinessLocationlist
                }
            }
            static void DisplayBusinessLocationlist(List<BusinessLocation> BusinessLocationlist) //Method to display Business Locations from BusinessLocationlist
            {
                Console.WriteLine("{0,-20} {1,-20} {2,-10} {3,-15}", "Business Name", "Branch Code", "Maximum Capacity", "Visitors Now");
                foreach (BusinessLocation bl in BusinessLocationlist)
                {

                    Console.WriteLine("{0,-20} {1,-20} {2,-16} {3,-15}", bl.businessName, bl.branchCode, bl.MaximumCapacity, bl.visitorsNow);
                }
            }
            static void SafeEntryCheckIn(List<Person> personList, List<BusinessLocation> BusinessLocationlist) //Method to allow users to check in
            {
                try //Try and Catch to check for wrong user input
                {
                    Console.WriteLine("Please enter your Name"); //Prompt user input for person Name
                    string targetname = Console.ReadLine();
                    if (Search(personList, targetname) != null) //Searches for person name in personList and makes sure that it exists in the list
                    {
                        DisplayBusinessLocationlist(BusinessLocationlist); //Display list of Business Locations
                        Console.WriteLine("Enter business location to check in: "); //Prompt user to key in Business Location to check in
                        string locationsearch = Console.ReadLine();
                        foreach (BusinessLocation bl in BusinessLocationlist)
                        {
                            if (bl.businessName == locationsearch) //Checks Business location entered by user and matches it with the existing business locations
                            {
                                Console.WriteLine("Location found: " + bl.businessName); //Returns name of business location if found

                                Person p = Search(personList, targetname); //Search function for person name
                                if (bl.IsFull() == false) //Call ISFull() method to check if business location is full
                                {
                                    Console.WriteLine("You have checked in " + bl.businessName + " location");
                                    DateTime CheckIn = DateTime.Now; //Initialize check in time
                                    DateTime CheckOut = DateTime.Now; //Initialize check out time
                                    SafeEntry se = new SafeEntry(CheckIn, CheckOut, bl); //Create new safeEntry object
                                    bl.visitorsNow = bl.visitorsNow += 1; //Increases visitor count of business location by 1
                                    p.AddSafeEntry(se); //Add new safeentry object into person
                                }
                            }
                            //else if (bl.businessName != locationsearch)
                            //{
                            //  Console.WriteLine("Business Location not found,please try again");
                            // break;
                            //}
                        }
                    }
                    else //Simple Validation
                    {
                        Console.WriteLine("Sorry the name is not in the list,please try again");
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }
            }
            static void SafeEntryCheckOut(List<Person> personList, List<BusinessLocation> BusinessLocationlist) //Method to allow users to perform checkout
            {
                try //Try and Catch to check for wrong user input
                {
                    Console.WriteLine("Please enter your Name"); //Prompt user to enter name
                    string targetname = Console.ReadLine();
                    if (Search(personList, targetname) != null) //Searches for person name in personList and makes sure that it exists in the list
                    {
                        Person p = Search(personList, targetname); //Initializes person name
                        foreach (SafeEntry se in p.safeEntryList) //Loops through SafeEntry list 
                        {
                            if (se.checkOut != null) //Checks if user has not perform checkout yet
                            {
                                Console.WriteLine("Check In Time: " + se.checkIn + "\tBusiness location:" + se.location.businessName);

                            }
                            else if(se.checkIn == null) //Check if user has already performed checkOut
                            {
                                Console.WriteLine("You have no records to check out");
                            }
                            else
                            {
                                Console.WriteLine("You have no records to check out");
                            }
                        }
                        Console.WriteLine("Choose the Business location to check out: "); //Prompt user to choose record to check out
                        string userlocation = Console.ReadLine();
                        BusinessLocation bl = Searchbusinesslocation(BusinessLocationlist, userlocation); //Searches for business location that user entered
                        List<SafeEntry> safeentryremovelist = new List<SafeEntry>(); //Initialize new remove list to allow user to remove safeEntry record
                        foreach (SafeEntry se in p.safeEntryList)
                        {
                            if (se.location == bl) //Checks if user input is in the business location list
                            {
                                se.PerformCheckout(); //Call performCheckout method 
                                se.checkOut = DateTime.Now;  //Initialize checkout time to current time
                                Console.WriteLine("Your checkout time: " + se.checkOut);
                                bl.visitorsNow = bl.visitorsNow - 1; //Reduce visitors Now in Business Location by 1
                                safeentryremovelist.Add(se); //add SafeEntry record user wants to remove in the new remove list

                            }
                            else
                            {
                                Console.WriteLine("This location is not found, please try again"); //Checks if user input location is valid
                            }
                        }
                        foreach (SafeEntry record in safeentryremovelist)
                        {
                            p.safeEntryList.Remove(record); //Remove safeEntry record in safeEntry List
                        }
                    }
                    else
                    {
                        Console.WriteLine("The person is not found"); //Checks if person entered by the user is valid
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }
            }
            static void Listallvisitors(List<Visitor> visitorlist,List<Person>personList) //Displays all details of all the visitors in visitorlist
            {
                foreach (Person p in personList)
                {
                    if (p is Visitor)
                    {
                        Console.WriteLine();
                        Visitor v = (Visitor)p;
                        Console.WriteLine(v.ToString());
                    }
                }
            }
            static void AssignReplaceToken(List<Resident> residentlist) //Method to allow replacement of Tokens for residents
            {
                try //Try and Catch for possible format errors
                {
                    Console.WriteLine("Please enter your name (Residents Only): "); //Prompt user to key in name of resident
                    string targetresident = Console.ReadLine();
                    if (Searchresident(residentlist, targetresident) != null)//Searches for resident in the residentlist
                    {
                        Resident r = Searchresident(residentlist, targetresident);
                        if (r.token == null) //Checks if resident has a TraceTogether Token
                        {
                            Console.WriteLine("You do not have a TraceTogether Token, please create a new one"); //Prompts user to key in details for to assign a new tracetogether token to the resident
                            Console.WriteLine("Please enter your Serial number: ");  
                            string sN = Console.ReadLine();
                            Console.WriteLine("Please enter your Collection Location: ");
                            string cL = Console.ReadLine();
                            DateTime eD = DateTime.Now.AddMonths(6); //Adds 6 months to the expiry date of the new TraceTogether Token
                            TraceTogetherToken newtoken = new TraceTogetherToken(sN, cL, eD); //Create new Token object
                            r.token = newtoken;
                            Console.WriteLine("Your new token has been created");
                        }
                        else if (r.token != null) //Checks if resident already has an existing token and wants to replace it
                        {
                            if (r.token.isEligibleForReplacement() == true) //Checks if resident's token is eligible for replacement using the IsElligibleForReplacement method
                            {
                                Console.WriteLine("Please enter your new serial number");
                                string newserial = Console.ReadLine();
                                Console.WriteLine("Please enter your new collection location");
                                string newcollectionlocation = Console.ReadLine();
                                r.token.ReplaceToken(newserial, newcollectionlocation);
                            }
                            else if ((r.token.expiryDate - DateTime.Now).Days! <= 30) //Checks if Token has expired
                            {
                                Console.WriteLine("Your are unable to replace your token");
                            }
                        }
                       
                    }
                    else  //Simple Validation
                    {
                        Console.WriteLine("This resident does not exist, please try again");
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }

            }
            static Person Search(List<Person> personList, string targetname) //Search for Person in personList method
            {
                foreach (Person p in personList)
                {
                    if (p.Name == targetname) //Check if name exists
                    {
                        return p;
                    }

                }
                return null;
            }
            static Resident Searchresident(List<Resident> residentlist, string targetresident) //Search for resident in residentlist method
            {
                for (int i = 0; i < residentlist.Count; i++)
                {
                    Resident p = residentlist[i];

                    if (p.Name == targetresident) //Check if name exists
                    {
                        return p;
                    }

                }
                return null;
            }
            static BusinessLocation Searchbusinesslocation(List<BusinessLocation> BusinessLocationlist, string userlocation) //Search Method for Business Locations
            {
                for (int i = 0; i < BusinessLocationlist.Count; i++)
                {
                    BusinessLocation bl = BusinessLocationlist[i];

                    if (bl.businessName == userlocation) //Check if Business Location exists
                    {
                        return bl;
                    }

                }
                return null;
            }
            static SHNFacility Searchshnfacility(List<SHNFacility> SHNFacilityList, string userfacility) //Search Method for SHNFacilities
            {
                for (int i = 0; i < SHNFacilityList.Count; i++)
                {
                    SHNFacility shn = SHNFacilityList[i];

                    if (shn.FacilityName == userfacility) //Check if SHNFacility exists
                    {
                        return shn;
                    }

                }
                return null;
            }
            static Visitor Searchvisitor(List<Visitor> visitorlist, string targetvisitor) //Search Method for Visitor
            {
                for (int i = 0; i < visitorlist.Count; i++)
                {
                    Visitor v = visitorlist[i]; 

                    if (v.Name == targetvisitor) //Check if visitor exists
                    {
                        return v;
                    }

                }
                return null;
            }
            static void CalculateSHNCharges(List<Person> personList) //Method to calculate SHN Charges
            {
                try //Try and Catch for formating errors
                {
                    Console.WriteLine("Please enter your name:"); //Prompt user to enter name to search
                    string targetname = Console.ReadLine();
                    Person p = Search(personList, targetname);
                    if (Search(personList, targetname) != null) //Checks if name exists in personList
                    {
                        foreach (TravelEntry te in p.travelEntryList)
                        {
                            if (!te.IsPaid) //Check if User has not paid for the SHN Stay
                            {
                                if (te.ShnEndDate < DateTime.Now) //Check if user has exceeded the SHNEndDate 
                                {
                                    double Total = p.CalculateSHNCharges();
                                    if (te.ShnStay != null) //Check if user has a SHNStay Record
                                    {
                                        double Transportationcost = te.ShnStay.CalculateTransportation(te.EntryMode, te.EntryDate); //Method for calculating transportation
                                        Total = Total + Transportationcost;
                                    }
                                    Total = Math.Ceiling(Total * 1.07);
                                    Console.WriteLine("Please make your payment for your SHN stay: " + Total); //Prompt user to key in total amount to pay
                                    double payment = Convert.ToDouble(Console.ReadLine());
                                    if (payment == Total)
                                    {
                                        Console.WriteLine("You have paid the full amount"); //Check if user has paid the full amount
                                        te.IsPaid = true;
                                    }
                                    else
                                    {
                                        Console.WriteLine("You have not paid the full amount,please pay the remaining amount"); //prompt user to key in remaining amount if not paid in full
                                        te.IsPaid = false;
                                    }

                                }

                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("Person is not found"); //Simple Validation
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }
            }
            static void EditBusinessCapacity(List<BusinessLocation> BusinessLocationlist) //Method to allow users to edit business capacity
            {
                try
                {
                    Console.WriteLine("Enter the name of the business location: "); //Prompt user to enter name of business Location
                    string userlocation = Console.ReadLine();
                    BusinessLocation location = Searchbusinesslocation(BusinessLocationlist, userlocation); //Search Method for Business Location
                    if (location != null)
                    {
                        Console.WriteLine("Edit maximum capacity: "); //Prompt user to edit maximum capacity for the business location they want
                        int UserCapacityInput = Convert.ToInt32(Console.ReadLine());
                        if (UserCapacityInput < 0)
                        {
                            Console.WriteLine("That is not a valid capacity input");
                        }
                        else
                        {
                            location.MaximumCapacity = UserCapacityInput; //Update business location maximum capacity with user input
                            Console.WriteLine("The new maximum capacity for this location is updated");
                        }
                        
                    }
                    else
                    {
                        Console.WriteLine("The location is not found, please try again"); //Simple Validation
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }
            }
            static void CreateVisitor(List<Person> personList, List<Visitor> visitorlist) //Method to allow users to create a new visitor
            {
                try //Try and Catch for format errors
                { 
                    Console.WriteLine("Enter your Name"); //Prompt user to enter details to create new Visitor
                    string name = Console.ReadLine();
                    Console.WriteLine("Enter your passport Number");
                    string passportNumber = Console.ReadLine();
                    Console.WriteLine("Enter your Nationality");
                    string nationality = Console.ReadLine();
                    Visitor newvisitor = new Visitor(name, passportNumber, nationality);
                    personList.Add(newvisitor);
                    visitorlist.Add(newvisitor);
                    Console.WriteLine("New Visitor is created");
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format, please try again");
                }
            }
            static void loadpersondetails(List<Person> personList, List<Visitor> visitorlist, List<Resident> residentlist) //Method to load Person details
            {
                try //Try and Catch for possible format errors 
                {
                    Console.WriteLine("Enter your type"); //Prompt user to enter details to search for person
                    string usertype = Console.ReadLine();
                    Console.WriteLine("Enter your name");
                    string targetname = Console.ReadLine();
                    string[] lineArray = File.ReadAllLines("Person.csv"); //Reads from person.csv file to access information of the person
                    for (int i = 1; i < lineArray.Length; i++)
                    {
                        string[] dataArray = lineArray[i].Split(',');
                        string type = dataArray[0];

                    }
                    if (usertype == "resident") //Checks if person entered is a resident
                    {
                        for (int i = 1; i < lineArray.Length; i++)
                        {
                            string[] dataArray = lineArray[i].Split(',');
                            string tokensn = dataArray[6];
                            string tokenlocation = dataArray[7];
                            string exdate = dataArray[8];
                            Resident r = Searchresident(residentlist, targetname);
                            if (targetname == dataArray[1]) //Checks if user input name matches with names in the residentlist
                                {
                                    if (r.token != null) //Checks if resident has a tracetogether token
                                    {
                                        Console.WriteLine("Tracetogether Token Information: "); //If resident, displays tracetogether token information
                                        Console.WriteLine("Trace Together Token Serial Number:" + r.token.serialNo + "\t collection location: " + r.token.collectionLocation + "\t Trace Together token expiry date: " + r.token.expiryDate);
                                        Console.WriteLine("Safe Entry Information: "); //Display SafeEntry Information
                                        foreach (SafeEntry se in r.safeEntryList)
                                        {
                                            Console.WriteLine("SafeEntry CheckIn Time: " + se.checkIn + "\tSafeEntry CheckIn Location Name: " + se.location.businessName);
                                        }
                                        Console.WriteLine("Travel Entry Information: "); //Display TravelEntry Information
                                        foreach (TravelEntry te in r.travelEntryList)
                                        {
                                            if (te.ShnStay == null) //Check if resident has stayed in a SHNFacility
                                            {
                                                Console.WriteLine("Last Country of Embarktion: " + te.LastCountryOfEmbarktion + "\t Entry Mode: " + te.EntryMode + "\t Entry Date:" + te.EntryDate);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Last Country of Embarktion: " + te.LastCountryOfEmbarktion + "\t Entry Mode: " + te.EntryMode + "\t Entry Date:" + te.EntryDate + "\t SHN Facility: " + te.ShnStay.FacilityName);
                                            }
                                        }
                                    }
                                    else if (r.token == null) //Check if resident does not have a tracetogether token
                                    {
                                        Console.WriteLine("Safe Entry Information: "); //Display SafeEntry Information
                                        foreach (SafeEntry se in r.safeEntryList)
                                        {
                                            Console.WriteLine("SafeEntry CheckIn Time: " + se.checkIn + "\tSafeEntry CheckOut Time:" + se.checkOut + "\tSafeEntry Checkout Location Name: " + se.location.businessName);
                                        }
                                        Console.WriteLine("Travel Entry Information: "); //Display SafeEntry Information
                                        foreach (TravelEntry te in r.travelEntryList)
                                        {
                                            if (te.ShnStay == null) //Checks if user has not stayed in a SHNFacility
                                            {
                                                Console.WriteLine("Last Country of Embarktion: " + te.LastCountryOfEmbarktion + "\t Entry Mode: " + te.EntryMode + "\t Entry Date:" + te.EntryDate);
                                            }
                                            else
                                            {
                                                Console.WriteLine("Last Country of Embarktion: " + te.LastCountryOfEmbarktion + "\t Entry Mode: " + te.EntryMode + "\t Entry Date:" + te.EntryDate + "\t SHN Facility: " + te.ShnStay.FacilityName);
                                            }
                                        }
                                    }
                                }
                            
                        }
                    }
                    else //Checks if user is a visitor
                    {
                        Visitor v = Searchvisitor(visitorlist, targetname); //Searches for visitor name
                        if (v != null) //Check if visitor name exists in the list
                        {
                            Console.WriteLine("Name: " + v.Name);
                            Console.WriteLine("Safe Entry Information: ");  //Display SafeEntry Information
                            foreach (SafeEntry se in v.safeEntryList)
                            {
                                Console.WriteLine("SafeEntry CheckIn Time: " + se.checkIn + "\tSafeEntry CheckOut Time: " + se.checkOut + "\tSafeEntry Checkout Location: " + se.location.businessName);
                            }
                            Console.WriteLine("Travel Entry Information: ");  //Display TravelEntry Information
                            foreach (TravelEntry te in v.travelEntryList)
                            {
                                Console.WriteLine("Last Country of Embarktion: " + te.LastCountryOfEmbarktion + "\t Entry Mode: " + te.EntryMode + "\t Entry Date:" + te.EntryDate);
                            }
                        }
                        else
                        {
                            Console.WriteLine("No person is found"); //Simple Validation
                        }

                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format");
                }

            }
            static void CreateContactTracingReportingCSV(List<Visitor> visitorlist, List<BusinessLocation> BusinessLocationlist, List<Person> personList) //Method to create a new csv to store Contact Tracing Reports (Advanced Feature)
            {
                try
                { 
                    Console.WriteLine("Please Enter your Start date in YY/MM/DD Format: "); //Prompt user to enter SafeEntry information to store
                    DateTime startdate = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please Enter your End date in YY/MM/DD Format: ");
                    DateTime enddate = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please Enter your Business Location");
                    string userlocation = Console.ReadLine();
                    BusinessLocation bl = Searchbusinesslocation(BusinessLocationlist, userlocation); //Search for business location
                    foreach (Person p in personList)
                    {
                        foreach (SafeEntry se in p.safeEntryList)
                        {
                            if (bl == se.location) //Checks if safeEntry location that user is checked in matches with the business location
                            {
                                if (se.checkIn >= startdate && se.checkIn <= enddate) //Check if user's check in time is between the start date and end date
                                {
                                    string data = p.Name + "," + se.checkIn + "," + se.checkOut + "," + bl.businessName; //Prints out information to a csv
                                    using (StreamWriter sw = new StreamWriter("ContactTracing.csv", true))
                                    {
                                        sw.WriteLine(data);
                                    }
                                }

                            }

                        }
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format");
                }
            }
            static void CreateStatusReport(List<SHNFacility> SHNFacilityList, List<Person> personList) //Method to create a status report (Advanced Feature)
            {
                try
                {
                    foreach (Person p in personList)
                    {
                        foreach (TravelEntry te in p.travelEntryList)
                        {
                            if (te.LastCountryOfEmbarktion != "New Zealand" && te.LastCountryOfEmbarktion != "Vietnam") //Check for user's last country of embarktion
                            {
                                if (te.ShnStay != null)
                                {
                                    string data = p.Name + "," + te.ShnEndDate + "," + te.ShnStay.FacilityName; //Writes data into a new csv file
                                    using (StreamWriter sw = new StreamWriter("StatusReport.csv", true))
                                    {
                                        sw.WriteLine(data);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Error: You have entered the wrong format");
                }
            }
            static void CreateTravelEntryRecord(List<Person> personList, List<SHNFacility> SHNFacilityList) //Method to allow users to create a new travelEntry Record
            {
                try {
                    Console.WriteLine("Please enter your name:"); //Prompt user for name
                    string targetname = Console.ReadLine();
                    Search(personList, targetname); //Search person according to user input
                    if (Search(personList, targetname) != null) //Ensure that person exists in the list
                    {
                        Console.WriteLine("Please enter your last country of embarktion: "); //Prompt user to key in Travel Entry Information
                        string userllcoe = Console.ReadLine();
                        Console.WriteLine("Please enter your entry mode");
                        string userentrymode = Console.ReadLine();
                        if(userentrymode != "Land" && userentrymode != "Sea" && userentrymode != "Air")
                        {
                            Console.WriteLine("That is not a valid user entry mode"); //Simple Validation
                        }
                        DateTime userentrydate = DateTime.Now;
                        TravelEntry newentry = new TravelEntry(userllcoe, userentrymode, userentrydate); //Create new TravelEntry Object
                        Person p = Search(personList, targetname); 
                        newentry.CalculateSHNDuration(); //Call method to calculate shnDuration
                        if (newentry.CalculateSHNDuration() > 0) //Check if user has indeed been assigned a SHN Duration
                        {
                            DisplaySHNFacilityList(SHNFacilityList); //Display list of SHN Facilities
                            Console.WriteLine("Please choose a Facility"); //Prompt user to choose 1 facility
                            string userfacility = Console.ReadLine();
                            SHNFacility shn = Searchshnfacility(SHNFacilityList, userfacility);
                            while (!shn.IsAvailable()) //Call is available method to check if SHNFacility is not available
                            {
                                Console.WriteLine("SHNFacility is not available, please choose again"); 
                                userfacility = Console.ReadLine();
                                shn = Searchshnfacility(SHNFacilityList, userfacility);
                            }
                            newentry.AssignSHNFacility(shn); //Call assign shn facility to assign facility to user
                            Console.WriteLine("The Facility you are staying in is: " + shn);
                            shn.FacilityVacancy = shn.FacilityVacancy - 1; //Reduce SHN facility vacancy by 1
                            foreach (TravelEntry te in p.travelEntryList)
                            {
                                te.ShnStay = shn;
                                te.IsPaid = false;
                            }
                        }
                        p.AddTravelEntry(newentry); //Add new travel entry record into personList
                        Console.WriteLine("You have created a new travel entry record");
                    }
                    else
                    {
                        Console.WriteLine("The Person is not found"); //Simple Validaition
                    }
                }
                catch (FormatException ex) //Try and Catch for possible format errors
                {
                    Console.WriteLine("Error: You have entered the wrong format");
                }
            }
        }
    }


























}

