﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class TravelEntry
    {
        //attributes
        public string LastCountryOfEmbarktion { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool IsPaid { get; set; }
        //end of attributes

        //constructors
        public TravelEntry() { }
        public TravelEntry(string l, string em, DateTime ed)
        {
            LastCountryOfEmbarktion = l;
            EntryMode = em;
            EntryDate = ed;
        }
        //end of constructors

        //methods
        public void AssignSHNFacility(SHNFacility s)
        {
            ShnStay = s;
        }

        public int CalculateSHNDuration() //Calculate SHN Duration to spend by checking country
        {
            if (LastCountryOfEmbarktion == "New Zealand" || LastCountryOfEmbarktion == "Vietnam")
            {
                ShnEndDate = EntryDate.AddDays(0);
                return 0;
            }
            else if (LastCountryOfEmbarktion == "Macao SAR")
            {
                ShnEndDate = EntryDate.AddDays(7);
                return 7;
            }
            else
            {
                ShnEndDate = EntryDate.AddDays(14);
                return 14;
            }


        }

        public override string ToString() //ToString() Method
        {
            return "Last Country of Embarkation: " + LastCountryOfEmbarktion + "\nEntry Mode: " + EntryMode + "\nEntry Date: " + EntryDate;
        }
    }
}
