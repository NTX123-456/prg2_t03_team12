﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class SafeEntry //attributes
    {
        public DateTime checkIn { get; set; }
        public DateTime checkOut { get; set; }
        public BusinessLocation location { get; set; }
        //end of attributes
        public SafeEntry() { } //constructors
        public SafeEntry(DateTime CI, DateTime CO, BusinessLocation loc)
        {
            checkIn = CI;
            checkOut = CO;
            location = loc;
        }
        public override string ToString() //Tostring() Method
        {
            return "Check In time: " + checkIn +
                "\t CheckOut time: " + checkOut +
                "\t Business Locations: " + location;

        }


        public void PerformCheckout() //Method to allow user to Perform CheckOut
        {
            checkOut = DateTime.Now;
            Console.WriteLine("You have checked out");

        }


    }
}
