﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class Visitor : Person //Set Attributes
    {
        public string passportNo { get; set; }
        public string nationality { get; set; }
        //end of attributes
        public Visitor(string n, string pN, string nat) : base(n) //constructors
        {
            passportNo = pN;
            nationality = nat;
        }
        public override double CalculateSHNCharges() //Calculate SHNCharges Method for visitor
        {
            double total = 0;
            foreach (TravelEntry e in travelEntryList)
            {
                int days = e.CalculateSHNDuration();
                if (days == 7)
                {
                    total += 280;
                }
                else if (days == 14)
                {
                    total += 2280;
                }
            }
            return total;
        }

        public override string ToString() //ToString() Method
        {
            return "Name: " + Name +
                "\t Nationality: " + nationality +
                "\t Passport Number: " + passportNo;

        }
    }
}
