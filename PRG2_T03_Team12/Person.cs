﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    abstract class Person //Set Attributes ,abstract person class to inherit visitor and resident
    {
        public string Name { get; set; }
        public List<SafeEntry> safeEntryList { get; set; }
        public List<TravelEntry> travelEntryList { get; set; }

        public Person() { safeEntryList = new List<SafeEntry>(); travelEntryList = new List<TravelEntry>(); }
        //constructors


        public Person(string n) 
        {
            Name = n;
            safeEntryList = new List<SafeEntry>();
            travelEntryList = new List<TravelEntry>();
        }
        public void AddSafeEntry(SafeEntry se) { safeEntryList.Add(se); } //Set travelentry and safeentry lists
        public void AddTravelEntry(TravelEntry te) { travelEntryList.Add(te); }

        public abstract double CalculateSHNCharges(); //Abstract calculate shn charges

        public override string ToString()
        {
            return "Name: " + Name +
                "\t SafeEntry List: " + safeEntryList +
                "\t TravelEntry List: " + travelEntryList;

        }


    }
}
