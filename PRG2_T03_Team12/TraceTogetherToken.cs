﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class TraceTogetherToken //attributes
    {
        public string serialNo { get; set; }
        public string collectionLocation { get; set; }
        public DateTime expiryDate { get; set; }

        public TraceTogetherToken() { } //end of attributes
        public TraceTogetherToken(string sN, string cL, DateTime eD)
        {
            serialNo = sN;
            collectionLocation = cL;
            expiryDate = eD;
        }
        public bool isEligibleForReplacement() //Method to check if token of resident is eligible for replacement
        {
            DateTime currentDate = DateTime.Now;
            if ((expiryDate - currentDate).Days <= 30 && (expiryDate - currentDate).Days > 0)
            {
                Console.WriteLine("You are eligible to replace your token");
                return true;
            }
            else if ((expiryDate - currentDate).Days! > 0)
            {
                Console.WriteLine("You not eligible to replace your token, your expiry date is more than one month away");
                return false;
            }
            else
            {
                Console.WriteLine("You are not eligible to replace your token,your token has expired");
                return false;
            }
        }

        public void ReplaceToken(string sN, string cL) //Replace Token method
        {
            serialNo = sN;
            collectionLocation = cL;
            expiryDate = DateTime.Now.AddMonths(6);


        }
        public override string ToString() //ToString() Method
        { 
            return "Serial Number: " + serialNo +
                "\t Collection location: " + collectionLocation +
                "\tExpiry date: " + expiryDate;

        }
    }
}
