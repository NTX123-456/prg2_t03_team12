﻿using System;
using System.Collections.Generic;
using System.Text;

//============================================================
// Student Number : S10205325, S10182481
// Student Name : Ng Tung Xuan, Muhammad Faizal B Bathurul J 
// Module Group : T03
//============================================================

namespace PRG2_T03_Team12
{
    class SHNFacility
    {
        //attributes
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckPoint { get; set; }
        public double DistFromSeaCheckPoint { get; set; }
        public double DistFromLandCheckPoint { get; set; }
        //end of attributes

        //constructors
        public SHNFacility() { }
        public SHNFacility(string fn, int fc, double da, double ds, double dl)
        {
            FacilityName = fn;
            FacilityCapacity = fc;
            DistFromAirCheckPoint = da;
            DistFromSeaCheckPoint = ds;
            DistFromLandCheckPoint = dl;
        }
        //end of constructors

        //methods
        public bool IsAvailable()
        {
            if (FacilityVacancy >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public double CalculateTransportation(string entryMode, DateTime entryDate)
        {
            double total = 0;
            int basefare = 50;
            double surcharge = 0;
            if (entryMode == "Air")
            {
                total = basefare + DistFromAirCheckPoint * 0.22;

            }
            else if (entryMode == "Sea")
            {
                total = basefare + DistFromSeaCheckPoint * 0.22 + surcharge;
            }
            else if (entryMode == "Land")
            {
                total = basefare + DistFromLandCheckPoint * 0.22 + surcharge;
            }
            int hour = entryDate.Hour;
            if (hour < 6 && hour > 9 || hour < 6 && hour > 12)
            {
                surcharge = 1.25 * 50;
            }
            else if (hour < 0 && hour < 6)
            {
                surcharge = 1.5 * 50;
            }
            return total;


        }

        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\nFacility Capacity: " + FacilityCapacity + "\nDistance from Air Checkpoint: " + DistFromAirCheckPoint + "\nDistance from Sea Checkpoint: " + DistFromSeaCheckPoint + "\nDistance from Land Checkpoint: " + DistFromLandCheckPoint;
        }
    }
}
